function H = homography(v_in, v_out)
  A_in = v_in(:, 1);
  B_in = v_in(:, 2);
  C_in = v_in(:, 3);
  D_in = v_in(:, 4);

  A_out = v_out(:, 1);
  B_out = v_out(:, 2);
  C_out = v_out(:, 3);
  D_out = v_out(:, 4);

  % rows
  r_1 = [A_in(1) A_in(2) 1 0 0 0 0 0 0];
  r_2 = [0 0 0 A_in(1) A_in(2) 1 0 0 0];
  r_3 = [0 0 0 0 0 0 A_in(1) A_in(2) 1];

  r_4 = [B_in(1) B_in(2) 1 0 0 0 0 0 0];
  r_5 = [0 0 0 B_in(1) B_in(2) 1 0 0 0];
  r_6 = [0 0 0 0 0 0 B_in(1) B_in(2) 1];

  r_7 = [C_in(1) C_in(2) 1 0 0 0 0 0 0];
  r_8 = [0 0 0 C_in(1) C_in(2) 1 0 0 0];
  r_9 = [0 0 0 0 0 0 C_in(1) C_in(2) 1];

  r_10 = [D_in(1) D_in(2) 1 0 0 0 0 0 0];
  r_11 = [0 0 0 D_in(1) D_in(2) 1 0 0 0];
  r_12 = [0 0 0 0 0 0 D_in(1) D_in(2) 1];

  X = [r_1; r_2; r_3; r_4; r_5; r_6;
       r_7; r_8; r_9; r_10; r_11; r_12];

  b = [A_out(1); A_out(2); A_out(3);
       B_out(1); B_out(2); B_out(3);
       C_out(1); C_out(2); C_out(3);
       D_out(1); D_out(2); D_out(3);];

  h = linsolve(X, b);

  H = [h(1) h(2) h(3);
       h(4) h(5) h(6);
       h(7) h(8) h(9)];
end
