function l = line_homogeneus(xline, yline)

p1 = [xline(1); yline(1); 1];
p2 = [xline(size(xline, 2)); yline(size(xline, 2)); 1];
l = cross(p1, p2);
l = l/l(3);
end
