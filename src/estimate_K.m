function [W_solved K] = ...
  estimate_K(im, debug)

vanishing_points = get_vanishing(im, 1, 1, 1, false);
v_hor = vanishing_points.x;
v_ver = vanishing_points.y;
v_z = vanishing_points.z;

syms w1 w2 w3 w4;
W = [w1 0 w2; 0 w1 w3; w2 w3 w4];
eq1 = v_hor' * W * v_ver == 0;
eq2 = v_hor' * W * v_z == 0;
eq3 = v_ver' * W * v_z == 0;

s = solve([eq1; eq2; eq3]);
s.w1 = subs(s.w1, w4, 1);
s.w2 = subs(s.w2, w4, 1);
s.w3 = subs(s.w3, w4, 1);

% get W
w1 = double(s.w1);
w2 = double(s.w2);
w3 = double(s.w3);
w4 = 1;
W_solved = [w1 0 w2; 0 w1 w3; w2 w3 w4];

% get K
K = inv(chol(W_solved));
K = K/K(9);

if debug
  fprintf("ESTIMATION OF K\nalpha_x: %f\nalpha_y: %f\nx0: %f\ny0: %f\nw0: %f\n",K(1), K(5), K(7), K(8), K(9));
end
end
