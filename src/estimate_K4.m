% - Extimate K using the H planar and three orthogonal vanishing points
% * if the IAC found is not positive definite an extimation of the nearest positive
%    definite found is done (thanks to an external module)
% * the extimated K found is then compared with the K calculated with natural camera hyp.

function K = ...
  estimate_K4(H, im, debug)

% open a file
fileID = fopen("out/K_extimation_procedure.txt", "w");

% get the vanishing points from x y z
vanishing_points = get_vanishing(im, 1, 1, 1, true);
vx = vanishing_points.x;
vy = vanishing_points.y;
vz = vanishing_points.z;

% get columns of H
h1 = H(:, 1);
h2 = H(:, 2);

% first system
eq1 = get_constraint_for_W(vx, vy);
eq2 = get_constraint_for_W(vx, vz);
eq3 = get_constraint_for_W(vy, vz);
eq4 = get_constraint_for_W(h1, h2);
eq5 = get_constraint_for_W(h1, h1) - get_constraint_for_W(h2, h2);

% try all combination to get a positive definite
eqs = [eq1; eq2; eq3; eq4; eq5];
combs = nchoosek([1 2 3 4 5], 4);
found_definite_positive = 0;
found_W = 0;

% check if with this equations we obtain a definite positive exists
for i = 1:size(combs, 1)
  set = combs(i, :);
  mySystem = ...
            [ eqs(set(1), :); ...
              eqs(set(2), :); ...
              eqs(set(3), :); ...
              eqs(set(4), :)];

  s = null(mySystem);
  W = [s(1)  0     s(4); ...
        0     s(2)  s(5); ...
       s(4)  s(5)  s(3)];

  %W = double(W/W(9));

  % check all the eiginvalues are > 0
  e = eig(W);
  is_positive = e(1) > 0 * e(2) > 0 * e(3) >0 ;

  fprintf(fileID, "IS POSITIVE DEFINITE: %d - EQS COMBINATION  %d %d %d %d - eigenvalues: %d %d %d\n", ...
    is_positive, set(1), set(2), set(3), set(4), e(1), e(2), e(3));

  % if is positive definite calculate K
  if is_positive
    found_definite_positive = 1;
    found_W = W;
    break;
  end
end

% if we found a W positive definite then calculate the K
if found_definite_positive
  K = chol(found_W);
  K = K/K(9);
else

  % extimate the best definite positive respect to K calculated with 3 constraints
  [~, K_3const] = estimate_K(im, false);
  fprintf(fileID, "K got from 3 constraint: ax =  %f; ay = %f; x0 = %f; y0 = %f;\n", ...
    K_3const(1), K_3const(5), K_3const(7), K_3const(8));
  best_K = [0 0 0; 0 0 0; 0 0 0];
  error = inf;

  for i=1:size(combs, 1)
    set = combs(i, :);
    mySystem = ...
              [ eqs(set(1), :); ...
                eqs(set(2), :); ...
                eqs(set(3), :); ...
                eqs(set(4), :)];

    s = null(mySystem);
    W = [ s(1)  0     s(4); ...
          0     s(2)  s(5); ...
          s(4)  s(5)  s(3)];

    %W = double(W/W(9));

    % get the nearest positive definite matrix
    W_exstimated = nearestSPD(W);
    %W_exstimated = W_exstimated/W_exstimated(9);
    K = chol(W_exstimated);
    K = K/K(9);

    % the params of K
    dax = abs(abs(K(1)) - abs(K_3const(1)));
    day = abs(abs(K(5)) - abs(K_3const(5)));
    dx0 = abs(abs(K(7)) - abs(K_3const(7)));
    dy0 = abs(abs(K(8)) - abs(K_3const(8)));
    K_err = (2*dax + 2*day + dx0 + dy0)/4;

    if(K_err < error)
        error = K_err;
        best_K = K;
    end
    fprintf(fileID, "ESTIMATION OF K: ax =  %f; ay = %f; x0 = %f; y0 = %f;\n", K(1), K(5), K(7), K(8));
  end
end

K = best_K;
fprintf(fileID, "BEST ESTIMATION OF K: ax =  %f; ay = %f; x0 = %f; y0 = %f;\n", K(1), K(5), K(7), K(8));
fclose(fileID);

% write solution
fileID = fopen("out/K_extimation.txt", "w");
fprintf(fileID, "3 constraint K (natural camera)\nax =  %f; ay = %f; x0 = %f; y0 = %f;\n\n", ...
    K_3const(1), K_3const(5), K_3const(7), K_3const(8));
fprintf(fileID, "4 constraint K (not natural camera)\nax =  %f; ay = %f; x0 = %f; y0 = %f;\n\n", ...
    K(1), K(5), K(7), K(8));

fprintf(fileID, "where K is like this:\n | ax  0  x0 |\n | 0  ay  y0 |\n | 0   0   1 |");
fclose(fileID);

% we adjust the W such to be positive definite
%e = eig(W_solved);
%solution = W_solved + max(-e(1),1)*eye(size(W_solved));
%K = chol(solution);
end
