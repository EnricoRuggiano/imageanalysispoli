function trapeze = ...
  get_trapeze(im, debug, regions)


str_left = regions(1);
str_right = regions(2);
str_up = regions(3);
str_down = regions(4);

  [xLeft yLeft cornersLeft]  = get_corner(im, str_left, false);
  [xRight yRight cornersRight] = get_corner(im, str_right, false);
  [xUp yUp cornersUp] = get_corner(im, str_up, false);
  [xDown yDown cornersDown] = get_corner(im, str_down, false);

  [xVertical1 yVertical1 ~] = get_corner(im, "vertical-2", false);
  [xVertical2 yVertical2 ~] = get_corner(im, "vertical-4", false);


  down = [xDown' yDown'];
  right = [xRight' yRight'];
  left = [xLeft' yLeft'];
  up = [xUp' yUp'];

  extra1 = [xVertical1' yVertical1'];
  extra2 = [xVertical2' yVertical2'];

if debug
  plot(xLeft, yLeft, 'LineWidth',1,'Color','y');
  plot(xRight, yRight, 'LineWidth',1,'Color','y');
  plot(xUp, yUp, 'LineWidth',1,'Color','y');
  plot(xDown, yDown, 'LineWidth',1,'Color','y');
  plot(xVertical1, yVertical1, 'LineWidth',1,'Color','y');
  plot(xVertical2, yVertical2, 'LineWidth',1,'Color','y');
end

  % get some vertices
  p1 = [xLeft(1); yLeft(1); 1];
  p2 = [xLeft(size(xLeft, 2)); yLeft(size(xLeft, 2)); 1];
  lineLeft = cross(p1, p2);

  p1 = [xRight(1); yRight(1); 1];
  p2 = [xRight(size(xRight, 2)); yRight(size(xRight, 2)); 1];
  lineRight = cross(p1, p2);

  p1 = [xUp(1); yUp(1); 1];
  p2 = [xUp(size(xUp, 2)); yUp(size(xUp, 2)); 1];
  lineUp = cross(p1, p2);

  p1 = [xDown(1); yDown(1); 1];
  p2 = [xDown(size(xDown, 2)); yDown(size(xDown, 2)); 1];
  lineDown = cross(p1, p2);

  p1 = [xVertical1(1); yVertical1(1); 1];
  p2 = [xVertical1(size(xVertical1, 2)); yVertical1(size(xVertical1, 2)); 1];
  lineExtra1 = cross(p1, p2);

  p1 = [xVertical2(1); yVertical2(1); 1];
  p2 = [xVertical2(size(xVertical2, 2)); yVertical2(size(xVertical2, 2)); 1];
  lineExtra2 = cross(p1, p2);

  % vertices
  a = cross(lineLeft, lineUp);
  b = cross(lineRight, lineUp);
  c = cross(lineRight, lineDown);
  d = cross(lineLeft, lineDown);

  e = cross(lineDown, lineExtra1);
  f = cross(lineDown, lineExtra2);
  g = cross(lineUp, lineExtra2);
  h = cross(lineUp, lineExtra1);


  a = a/a(3);
  b = b/b(3);
  c = c/c(3);
  d = d/d(3);
  e = e/e(3);
  f = f/f(3);
  g = g/g(3);
  h = h/h(3);

  % plot
  if debug
    plot([a(1) b(1) c(1) d(1)], ...
    [a(2) b(2) c(2) d(2)], 'bo');
  end

  % struct
  trapeze = struct();
  trapeze.a = a;
  trapeze.b = b;
  trapeze.c = c;
  trapeze.d = d;
  trapeze.e = e;
  trapeze.f = f;
  trapeze.g = g;
  trapeze.h = h;
  trapeze.lineLeft = lineLeft;
  trapeze.lineUp = lineUp;
  trapeze.lineRight = lineRight;
  trapeze.lineDown = lineDown;
  trapeze.lineExtra1 = lineExtra1;
  trapeze.lineExtra2 = lineExtra2;
  trapeze.down = down;
  trapeze.left = left;
  trapeze.right = right;
  trapeze.up = up;
  trapeze.extra1 = extra1;
  trapeze.extra2 = extra2;

end
