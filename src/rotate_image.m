function [out_image R] = ...
  rotate_image(im, points, debug)

% calculate the line between the two points
p1 = points(:, 1);
p2 = points(:, 2);
p1_ = points(:, 1);
p2_ = points(:, 2);
p1 = normalize_coord(im, p1, 0);
p2 = normalize_coord(im, p2, 0);

l = cross(p1, p2);
%l = l/l(3);
dp = [(p2(1) - p1(1)) (p2(2) - p1(2))];

% x axis
x0 = [1; 1; 1];
xend = [size(im, 2); 1; 1];
x0 = normalize_coord(im, x0, 0);
xend = normalize_coord(im, xend, 0);

lx = cross(x0, xend);
%lx = lx / lx(3);
dx = [(xend(1) - x0(1)) (xend(2) - x0(2))];

% calculate the angle between x axis and line
cosTheta = dot(l, lx) / (norm(dp) * norm(dx));
theta = acosd(cosTheta);

% rotate
R =[cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];

T = maketform('affine', R');
if debug
  out_image = imtransform(im, T);
else
  out_image = imtransform(im, T, 'XData',[1 size(im, 2)], 'YData',[1 size(im, 1)]);
end
% display
if debug
  figure();
  imshow(out_image);
end

% this is not correct
%out_image = imrotate(im, -theta);

end
