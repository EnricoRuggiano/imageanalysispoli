function cr = ...
  get_cross_ratio(a, b, c, d)

ac = distance(a, c);
bd = distance(b, d);
bc = distance(b, c);
ad = distance(a, d);

cr = (ac * bd)/(bc * ad);


end
