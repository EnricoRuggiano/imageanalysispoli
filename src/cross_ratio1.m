function [depth facade] = ...
  cross_ratio1(im, window_metric, debug)

% get my lines
[xVertical1 yVertical1 ~] = get_corner(im, "vertical-1", false);
[xVertical2 yVertical2 ~] = get_corner(im, "vertical-2", false);
[xVertical3 yVertical3 ~] = get_corner(im, "vertical-3", false);
[xVertical4 yVertical4 ~] = get_corner(im, "vertical-4", false);
[xWindow1 yWindow1 ~] = get_corner(im, "window-vertical-1", false);
[xWindow2 yWindow2 ~] = get_corner(im, "window-vertical-2", false);
[xDown1 yDown1 ~] = get_corner(im, "down", false);

vertical1 = [xVertical1' yVertical1'];
vertical2 = [xVertical2' yVertical2'];
vertical3 = [xVertical3' yVertical3'];
vertical4 = [xVertical4' yVertical4'];
window1 = [xWindow1' yWindow1'];
window2 = [xWindow2' yWindow2'];
down1 = [xDown1' yDown1'];

l_vertical1 = line_homogeneus(xVertical1, yVertical1);
l_vertical2 = line_homogeneus(xVertical2, yVertical2);
l_vertical3 = line_homogeneus(xVertical3, yVertical3);
l_vertical4 = line_homogeneus(xVertical4, yVertical4);
l_down1 = line_homogeneus(xDown1, yDown1);
l_window1 = line_homogeneus(xWindow1, yWindow1);
l_window2 = line_homogeneus(xWindow2, yWindow2);

a = point_homogeneus(l_vertical1, l_down1);
b = point_homogeneus(l_window1, l_down1);
c = point_homogeneus(l_window2, l_down1);
d = point_homogeneus(l_vertical2, l_down1);
e = point_homogeneus(l_vertical3, l_down1);
f = point_homogeneus(l_vertical4, l_down1);

% 1 - cross ratio
cr1 = get_cross_ratio(a, d, e, f);
cr2 = get_cross_ratio(b, c, d, e);
cr3 = get_cross_ratio(a, b, c, d);

% definitions of params
syms x z k w;
ab = x;
bc = w;
cd = z;
de = k;

% compositions
ef = x + w + z; % hyp

ac = ab + bc;
ad = ac + cd;
ae = ad + de;
af = ae + ef;
bd = bc + cd;
be = bd + de;
bf = be + ef;
ce = cd + de;
cf = ce + ef;
df = de + ef;

% symbolics cross ratio

% cr(a, d, e, f)
sym_cr1 = (ae * df) / (de * af);

% cr(b, c, d, e)
sym_cr2 = (bd * ce) / (cd * be);

% cr(a, b, c, d)
sym_cr3 = (ac * bd) / (bc * ad);

% equation
eq1 = sym_cr1 == cr1;
eq2 = sym_cr2 == cr2;
eq3 = sym_cr3 == cr3;

% solve
s = solve([eq1; eq2; eq3]);
w_result = double(subs(s.w, k, 1));
x_result = double(subs(s.x, k, 1));
z_result = double(subs(s.z, k, 1));

w_to_k = w_result(w_result>0);
x_to_k = x_result(x_result>0);
z_to_k = z_result(z_result>0);

facade_to_k = w_to_k + x_to_k + z_to_k;

% calculate k
k = window_metric / w_to_k;
facade = facade_to_k * k;
depth = k + 2 * facade;

if debug
 fig = figure();
 imshow(im);
 hold on

 plot(vertical1(:,1), vertical1(:,2), 'LineWidth',1,'Color','y');
 plot(vertical2(:,1), vertical2(:,2), 'LineWidth',1,'Color','y');
 plot(vertical3(:,1), vertical3(:,2), 'LineWidth',1,'Color','y');
 plot(vertical4(:,1), vertical4(:,2), 'LineWidth',1,'Color','y');
 plot(window1(:,1), window1(:,2), 'LineWidth',1,'Color','y');
 plot(window2(:,1), window2(:,2), 'LineWidth',1,'Color','y');
 plot(down1(:,1), down1(:,2), 'LineWidth',1,'Color','y');

 points_x = [a(1) b(1) c(1) d(1) e(1) f(1)];
 points_y = [a(2) b(2) c(2) d(2) e(2) f(2)];
 labels = {"A", "B", "C", "D", "E", "F"};

 plot(points_x, points_y, 'bo');
 text(double(points_x), double(points_y), labels, 'Color', 'b', 'FontSize', 14);
 title(['Distance extimation: AF = \fontsize{9}', num2str(depth), ' m']);
 print(fig, "out/cross_ratio1", '-dpng');

end


end
