function result = point_homogeneus(l1, l2)
p = cross(l1, l2);
result = p/p(3);
end
