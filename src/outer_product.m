function result = ...
  outer_product(a, b)

% from wikipedia
a1 = a(1);
a2 = a(2);
a3 = a(3);

b1 = b(1);
b2 = b(2);
b3 = b(3);

result = [a1*b1 a1*b2 a1*b3; ...
          a2*b1 a2*b2 a2*b3; ...
          a3*b1 a3*b2 a3*b3];

end
