function [left_space, left_facade] = ...
  cross_ratio2(im, window_metric, debug)

% get my lines
[xVertical1 yVertical1 ~] = get_corner(im, "left-vertical-1", false);
[xVertical2 yVertical2 ~] = get_corner(im, "left-vertical-2", false);
[xVertical3 yVertical3 ~] = get_corner(im, "left-vertical-3", false);
[xVertical4 yVertical4 ~] = get_corner(im, "vertical-1", false);
[xDown1 yDown1 ~] = get_corner(im, "left", false);

vertical1 = [xVertical1' yVertical1'];
vertical2 = [xVertical2' yVertical2'];
vertical3 = [xVertical3' yVertical3'];
vertical4 = [xVertical4' yVertical4'];
down1 = [xDown1' yDown1'];

l_vertical1 = line_homogeneus(xVertical1, yVertical1);
l_vertical2 = line_homogeneus(xVertical2, yVertical2);
l_vertical3 = line_homogeneus(xVertical3, yVertical3);
l_vertical4 = line_homogeneus(xVertical4, yVertical4);
l_down1 = line_homogeneus(xDown1, yDown1);

a = point_homogeneus(l_vertical1, l_down1);
b = point_homogeneus(l_vertical2, l_down1);
c = point_homogeneus(l_vertical3, l_down1);
d = point_homogeneus(l_vertical4, l_down1);

% 1 - cross ratio
cr1 = get_cross_ratio(a, b, c, d);

% definitions of params
syms x w;
ab = x;
bc = w;
cd = x; % hyp

% compositions

ac = ab + bc;
ad = ac + cd;
ad = ac + cd;
bd = bc + cd;

% symbolics cross ratio

% cr(a, b, c, d)
sym_cr1 = (ac * bd) / (bc * ad);

% equation
eq1 = sym_cr1 == cr1;

% solve

s = solve([eq1]);
x_result = double(subs(s, w, 1));
x_to_w = x_result(x_result>0);

left_space = window_metric * x_to_w ;

% calculate k
left_facade = window_metric + 2 * left_space;

if debug
 fig = figure();
 imshow(im);
 hold on

 plot(vertical1(:,1), vertical1(:,2), 'LineWidth',1,'Color','y');
 plot(vertical2(:,1), vertical2(:,2), 'LineWidth',1,'Color','y');
 plot(vertical3(:,1), vertical3(:,2), 'LineWidth',1,'Color','y');
 plot(vertical4(:,1), vertical4(:,2), 'LineWidth',1,'Color','y');
 plot(down1(:,1), down1(:,2), 'LineWidth',1,'Color','y');

 points_x = [a(1) b(1) c(1) d(1)];
 points_y = [a(2) b(2) c(2) d(2)];
 labels = {"A", "B", "C", "D"};

 plot(points_x, points_y, 'bo');
 text(double(points_x), double(points_y), labels, 'Color', 'b', 'FontSize', 14);
 title(['Distance extimation: BD = \fontsize{9}', num2str(left_space + window_metric), ' m']);
 print(fig, "out/cross_ratio2", '-dpng');

end


end
