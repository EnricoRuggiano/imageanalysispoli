% - Perform line detection.
% The result depends also on the fillGap and threshold passed.

function lines = ...
detect_lines(edges, FillGap, MinLength, debug)

% default parameters
if nargin < 3
  FillGap = 8;
  MinLength = 100;
  debug = false;
end

% hough lines
[H, theta, rho] = hough(edges);

P = houghpeaks(H,100);
lines = houghlines(edges,theta,rho,P, ...
  'FillGap', FillGap,'MinLength', MinLength);

if debug
  fig = figure();
  imshow(edges);
  hold on;
  max_len = 0;

  for k = 1:length(lines)
    xy = [lines(k).point1; lines(k).point2];
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

    % Plot beginnings and ends of lines
    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');

    % Determine the endpoints of the longest line segment
    len = norm(lines(k).point1 - lines(k).point2);
    if ( len > max_len)
        max_len = len;
        xy_long = xy;
    end

    % label
    str = num2str(k);
    text(xy(1,1), xy(1,2), str, 'Color', 'magenta', 'FontSize', 14);
    end

  % write the output figure
  title(['Detect Lines:\fontsize{9} FillGap: ', num2str(FillGap), ' MinLength: ', num2str(MinLength)]);
  print(fig, "out/lines", '-dpng');
  end
end
