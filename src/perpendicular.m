function [l, newLine] = ...
  perpendicular(p, myLine, thr)

% lets calculate m
xLine = myLine(1, :);
yLine = myLine(2, :);

x1 = xLine(1);
x2 = xLine(size(xLine, 2));
y1 = yLine(1);
y2 = yLine(size(xLine, 2));
m = abs((y1 - y2)/(x1 - x2));

% derivative
dy = (y2 - y1)/(x2 - x1);
if dy > 0
  m = -1/m;
else
  m = 1/m;
end


% lets get q from p
q = p(2) - m * p(1);
% lets calculate this line
xline = linspace(0, thr, 200);
yline = xline * m + q;

newLine = [xline; yline];

% lets calculate as homogeneus
p1 = [xline(1); yline(1); 1];
p2 = p;
l = cross(p1, p2);

end
