function n_point = denormalize_coord(image, point, bias)

% normalize points coordinates
n_point = [(point(1) * size(image, 2) + bias); ...
  (point(2) * size(image, 1)) + bias; 1];
