% - get a referement 3d axis usefull to plot
% get
function [O X Y Z] = ...
  get_axis3d(im, alpha, padding)

  On = [size(im, 2) * 0.25; size(im, 1) * 0.8; 0; 1];
  Xn = [On(1) + padding; On(2); On(3); 1];
  Yn = [On(1); On(2) - padding; On(3); 1];
  Zn = [On(1); On(2); On(3) - padding; 1];

  % lets rotate it such to see the z
   Ry = [cosd(alpha) 0 sind(alpha) 1; ...
        0          1      0      0; ...
      -sind(alpha) 0 cosd(alpha) 0; ...
        0          0      0      1];

   Rx = [ 1        0          0        0; ...
          0   cosd(alpha) -sind(alpha) 0; ...
          0   sind(alpha)  cosd(alpha) 0; ...
          0          0      0      1];

  T = [ 1 0 0 0;
        0 1 0 0;
        0 0 1 2 * padding;
        0 0 0 1];

% rotation along z axis
  Or = Ry * On;
  Xr = Ry * Xn;
  Yr = Ry * Yn;
  Zr = Ry * Zn;
  Or = Or/Or(4);
  Xr = Xr/Xr(4);
  Yr = Yr/Yr(4);
  Zr = Zr/Zr(4);

% rotation along x axis
  Ok = Rx * Or;
  Xk = Rx * Xr;
  Yk = Rx * Yr;
  Zk = Rx * Zr;
  Ok = Ok/Ok(4);
  Xk = Xk/Xk(4);
  Yk = Yk/Yk(4);
  Zk = Zk/Zk(4);

% translation on top of the image
  O = T * Ok;
  X = T * Xk;
  Y = T * Yk;
  Z = T * Zk;
  O = O/O(4);
  X = X/X(4);
  Y = Y/Y(4);
  Z = Z/Z(4);

end
