% Perform this moltiplication and get the linear constraint:
%                      [ w1  0   w4 ]   [ vy1 ]
% [ vx1  vx2  vx3 ] *  [  0  w2  w5 ] * [ vy2 ]
%                      [ w4  w5  w3 ]   [ vy3 ]

function eq = ...
  get_constraint_for_W(vx, vy)

vx1 = vx(1);
vx2 = vx(2);
vx3 = vx(3);

vy1 = vy(1);
vy2 = vy(2);
vy3 = vy(3);

% w1 w2 w3 w4 w5
eq = ...
  [ vx1 * vy1 ...
    vx2 * vy2 ...
    vx3 * vy3 ...
    vx1 * vy3 + vx3 * vy1 ...
    vx2 * vy3 + vx3 * vy2];
end
