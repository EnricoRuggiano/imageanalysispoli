% - Perform corner detection.
% Corner detection is performed after a gamma correction and an histeq on the
% input image

function corners = ...
  detect_corners(img_in, FilterSize,  gamma, eq, ROI, debug)

% default parameters
if nargin < 5
  FilterSize = 5;
  gamma = 1;
  eq = false;
  ROI = [1 1 size(im, 2) size(im, 1)];
end

% to grayscale
gray = rgb2gray(img_in);

% do some filtering
im = imadjust(gray, [], [], gamma);
if eq
  im = histeq(im);
  str_eq = 'true';
else
  str_eq = 'false';
end

corners = detectHarrisFeatures(im, 'FilterSize', FilterSize, 'ROI', ROI);

% plot this points

if debug
fig = figure();
imshow(img_in);
hold on

plot(corners.Location(:,1), corners.Location(:,2), 'ro');

% plot the labels
%s = size(corners.Location);
%for i=1:s(1)
%  x = double(corners.Location(i,1));
%  y = double(corners.Location(i,2));
%  str = num2str(i);
%  text(x, y, str, 'Color', 'magenta', 'FontSize', 5);
%end

% set a title with the parameters used
title(['Detect Corners:\fontsize{9} FilterSize: ', num2str(FilterSize), ' Gamma: ', num2str(gamma), ' Histeq: ', str_eq]);
print(fig, "out/corners", '-dpng');

end

end
