% - rectify using the K the lateral facade
% * compute the circular point as the intersection of IAC and linf
% * apply SVD to the dual conic

function [im_out, H] = ...
  rectify_from_K2(im, K, vanishing_points, dir1, dir2, K_3const, imscale, debug)

% select the vanishing points
v1 = 0;
v2 = 0;

% check same vanishing point
if strcmp(dir1, dir2)
  ME = MException("same vanishing point");
  throw(ME);
end

% get index of first vanishing point
if strcmp(dir1, "x")
  v1 = vanishing_points.x;
elseif strcmp(dir1, "y")
  v1 = vanishing_points.y;
elseif strcmp(dir1, "z")
  v1 = vanishing_points.z;
else
  ME = MException("Vanishing point %s not found", dir1);
  throw(ME);
end

% get index of second vanishing point
if strcmp(dir2, "x")
  v2 = vanishing_points.x;
elseif strcmp(dir2, "y")
  v2 = vanishing_points.y;
elseif strcmp(dir2, "z")
  v2 = vanishing_points.z;
else
  ME = MException("Vanishing point %s not found", dir2);
  throw(ME);
end

% linf
linf = cross(v1, v2);
linf = linf/linf(3);

% W
W = 0;

if K_3const
  [~, K_3const] = estimate_K(im, false);
  W = inv(K_3const * K_3const');
else
  W = inv(K * K');
end

% circular points
syms i1 i2;
w1 = W(1);
w4 = W(3);
w2 = W(5);
w5 = W(6);
w3 = W(9);
l1 = linf(1);
l2 = linf(2);
l3 = linf(3);

eq1 = i1^2 *w1 + i2^2*w2 + 2*i1*w4 + 2*i2*w5 + w3==0;
eq2 = i1*l1 + i2*l2+l3==0;

solution = solve([eq1; eq2]);

i1 = solution.i1(1);
i2 = solution.i2(1);
i1 = double(i1);
i2 = double(i2);

j1 = solution.i1(2);
j2 = solution.i2(2);
j1 = double(j1);
j2 = double(j2);

%calculate
I = [i1; i2; 1];
J = [j1; j2; 1];
Cinf = outer_product(I, J) + outer_product(J, I);

% svd
[U C V] = svd(Cinf);
H1 = U;

% lets rotate
theta = 110;
R = [cosd(theta) -sind(theta) 0; sind(theta) cosd(theta) 0; 0 0 1];
H = R * H1;

T1 = maketform('projective', H');
im_out = imtransform(im, T1, 'XYScale', imscale);

% lets crop it to best preview
ROI = [0 0.65*size(im_out,1) 0.35*size(im_out,2) 0.35*size(im_out,1)];
im_out_crop = imcrop(im_out, ROI);


if debug
  fig = figure();
  imshow(im_out_crop);
  title(['Plane yz rectification']);
  print(fig, "out/rectify_yz", '-dpng');
end
end
