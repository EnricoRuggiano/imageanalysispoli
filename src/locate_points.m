%
% - case plane xz
%
%   u = P * xw
%
%  so:
%  inv(P) * u = xw;
%

function res = ...
  locate_points (im_planar, P, M, R, t, plane, debug)

if strcmp(plane, 'xz')

% these are my points
A = [864; 203;  0; 1];
B = [843; 447; 0; 1];
C = [1040; 444; 0; 1];
D = [1033; 632; 0; 1];
E = [1280; 620; 0; 1];
F = [1294; 440; 0; 1];
G = [1480; 434; 0; 1];

if debug
  fig = figure();
  imshow(im_planar);
  hold on;
  points = [A B C D E F G];
  labels = {"A" "B" "C" "D" "E" "F" "G"};
  plot(points(1, :), points(2, :), 'or');
  text(double(points(1, :)), double(points (2, :)), labels, 'Color', 'b', 'FontSize', 14);
  title(['Points selected']);
  print(fig, "out/3d_reconstruction_points", '-dpng');
end

P_inv = inv([P; 0 0 0 1]);
a = P_inv * A;
b = P_inv * B;
c = P_inv * C;
d = P_inv * D;
e = P_inv * E;
f = P_inv * F;
g = P_inv * G;

a = a/a(4);
b = b/b(4);
c = c/c(4);
d = d/d(4);
e = e/e(4);
f = f/f(4);
g = g/g(4);

res = [a b c d e f g];


else
  ME = MException("Locate points: plane not implemented (use only xz)");
  throw(ME);
end

end
