% - Rectification of the plane xz
% * first use the cross ratio to get the metric distance between the z facades.
% * second use the cross ratio to get the metric distance between the window of
%     the lateral facade and the first window of frontal facade
% * estimate (with matlab maketform) the H to get from the metric to the imaged projection.

function [im_out, H, H2] = ...
  rectify6(im, scale_factor, debug)

  % get the region to rectify
  trapeze = get_trapeze(im, false, ["left" "right" "window-up" "down"]);

  % extract data from the struct trapeze
  d = trapeze.a;
  c = trapeze.b;
  b = trapeze.c;
  a = trapeze.d;
  e = trapeze.e;
  f = trapeze.f;
  g = trapeze.g;
  h = trapeze.h;

  lineLeft = trapeze.lineLeft;
  lineUp = trapeze.lineUp;
  lineRight = trapeze.lineRight;
  lineDown = trapeze.lineDown;
  lineExtra1 = trapeze.lineExtra1;
  lineExtra2 = trapeze.lineExtra2;

  down = trapeze.down;
  left = trapeze.left;
  right = trapeze.right;
  up = trapeze.up;


  if debug
   fig = figure();
   imshow(im);
   hold on
   plot(left(:,1), left(:,2), 'LineWidth',1,'Color','y');
   plot(right(:,1), right(:,2), 'LineWidth',1,'Color','y');
   plot(up(:,1), up(:,2), 'LineWidth',1,'Color','y');
   plot(down(:,1), down(:,2), 'LineWidth',1,'Color','y');

   points_x = [a(1) b(1) c(1) d(1)];
   points_y = [a(2) b(2) c(2) d(2)];
   labels = {"A", "B", "C", "D"};

   plot(points_x, points_y, 'bo');
   text(double(points_x), double(points_y), labels, 'Color', 'b', 'FontSize', 14);
   title(['Rectify this region']);
   print(fig, "out/rectify_region", '-dpng');

  end

  % apply cross ratio to get the metric lenght of the trapeze
  % hyp := window has size 1m
  window_metric = 1;
  [ratio_length, ~] = cross_ratio1(im, window_metric, true);
  [left_space, ~] = cross_ratio2(im, window_metric, true);
  ratio_window = window_metric + left_space;

  metric_a = [0; ratio_window; 1];
  metric_b = [ratio_length; ratio_window; 1];
  metric_c = [ratio_length; 0; 1];
  metric_d = [0; 0; 1];

  vin = [a b c d];
  vin1_temp = vin';
  vin1 = double(vin1_temp(:, 1:2));

  % write relative positions
  if debug
    fileID = fopen("out/positions.txt", "w");
    fprintf(fileID, "Projective Image\n A = [ %f; %f]; B = [ %f; %f]; C = [ %f; %f]; D = [ %f; %f];\n\n", ...
      a(1), a(2), b(1), b(2), c(1), c(2), d(1), d(2));
    fprintf(fileID, "Metric Image\n A = [ %f; %f]; B = [ %f; %f]; C = [ %f; %f]; D = [ %f; %f];\n\n", ...
      metric_a(1), metric_a(2), metric_b(1), metric_b(2), metric_c(1), metric_c(2), metric_d(1), metric_d(2));
    fclose(fileID);
  end


  vout = scale_factor * [metric_a metric_b metric_c metric_d];
  vout1_temp= vout';
  vout1 = double(vout1_temp(:, 1:2));

  % matlab projective
  T1 = maketform('projective', vin1, vout1);
  H = T1.tdata.T;
  im_out = imtransform(im, T1, 'XYScale', 1);

  % my projective => matlab maketform is more precise (optimizations?)
  H2_temp = double(homography(vin, vout));
  H2 = H2_temp / H2_temp(9);
  T2 = maketform('projective', H2');
  im_out2 = imtransform(im, T2, 'XYScale', 1/scale_factor);

if debug
  fig = figure();
  imshow(im_out);
  title(['Plane xz rectification']);
  print(fig, "out/rectify_xz", '-dpng');
end

end
