% - get the Vanishing points on the direction specified.
% three direction are used and only two vanishing points are available on those
% directions

function vanishing_points = ...
  get_vanishing(im, x_axis, y_axis, z_axis, debug)

  % get vanishing points for each axis
  vanishing_points = struct();
  fig = 0;

  if debug
    fig = figure();
    imshow(im);
    hold on;
  end

  if x_axis == 0
    vanishing_points.x = 0;
  elseif x_axis == 1
    [x_line1 y_line1 corners]  = get_corner(im, "up-last", false);
    [x_line2 y_line2 corners]  = get_corner(im, "down", false);
    x_l1 = line_homogeneus(x_line1, y_line1);
    x_l2 = line_homogeneus(x_line2, y_line2);
    v_hor1 = cross(x_l1, x_l2);
    v_hor1 = v_hor1/v_hor1(3);

    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','r');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','r');
      plot(v_hor1(1), v_hor1(2),'ro');
    end
    vanishing_points.x = v_hor1;
  elseif x_axis == 2
    [x_line1 y_line1 corners]  = get_corner(im, "up-last", false);
    [x_line2 y_line2 corners]  = get_corner(im, "down", false);
    [x_line3 y_line3 corners]  = get_corner(im, "up-first", false);
    [x_line4 y_line4 corners]  = get_corner(im, "up-second", false);

    x_l1 = line_homogeneus(x_line1, y_line1);
    x_l2 = line_homogeneus(x_line2, y_line2);
    x_l3 = line_homogeneus(x_line3, y_line3);
    x_l4 = line_homogeneus(x_line4, y_line4);

    v_hor1 = cross(x_l1, x_l2);
    v_hor2 = cross(x_l3, x_l4);
    v_hor1 = v_hor1/v_hor1(3);
    v_hor2 = v_hor2/v_hor2(3);

    vanishing_points.x = [v_hor1 v_hor2];
    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','r');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','r');
      plot(x_line3, y_line3, 'LineWidth',1,'Color','r');
      plot(x_line4, y_line4, 'LineWidth',1,'Color','r');
      plot(v_hor1(1), v_hor1(2),'ro');
      plot(v_hor2(1), v_hor2(2),'ro');
    end

  else
    ME = MException("Vanishing point of x-axis: supported args are 1 or 2");
    throw(ME);
  end

  if y_axis == 0
    vanishing_points.y = 0;
  elseif y_axis == 1

    [x_line1 y_line1 corners]  = get_corner(im, "vertical-1", false);
    [x_line2 y_line2 corners]  = get_corner(im, "vertical-3", false);
    y_l1 = line_homogeneus(x_line1, y_line1);
    y_l2 = line_homogeneus(x_line2, y_line2);
    v_ver1 = cross(y_l1, y_l2);
    v_ver1 = v_ver1/v_ver1(3);

    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','b');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','b');
      plot(v_ver1(1), v_ver1(2),'bo');
    end
    vanishing_points.y = v_ver1;
  elseif y_axis == 2

    [x_line1 y_line1 corners]  = get_corner(im, "vertical-1", false);
    [x_line2 y_line2 corners]  = get_corner(im, "vertical-2", false);
    [x_line3 y_line3 corners]  = get_corner(im, "vertical-3", false);
    y_l1 = line_homogeneus(x_line1, y_line1);
    y_l2 = line_homogeneus(x_line2, y_line2);
    y_l3 = line_homogeneus(x_line3, y_line3);
    v_ver1 = cross(y_l1, y_l2);
    v_ver2 = cross(y_l2, y_l3);
    v_ver1 = v_ver1/v_ver1(3);
    v_ver2 = v_ver2/v_ver2(3);

    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','b');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','b');
      plot(x_line3, y_line3, 'LineWidth',1,'Color','b');
      plot(x_line4, y_line4, 'LineWidth',1,'Color','b');
      plot(v_ver1(1), v_ver1(2),'bo');
      plot(v_ver2(1), v_ver2(2),'bo');
    end
    vanishing_points.y = [v_ver1 v_ver2];
  else
    ME = MException("Vanishing point of y-axis: supported args are 1 or 2");
    throw(ME);
  end
  if z_axis == 0
    vanishing_points.z = 0;
  elseif z_axis == 1

    [x_line1 y_line1 corners]  = get_corner(im, "left", false);
    [x_line2 y_line2 corners]  = get_corner(im, "right", false);
    z_l1 = line_homogeneus(x_line1, y_line1);
    z_l2 = line_homogeneus(x_line2, y_line2);
    v_z1 = cross(z_l1, z_l2);
    v_z1 = v_z1/v_z1(3);
    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','g');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','g');
      plot(v_z1(1), v_z1(2),'go');
    end
    vanishing_points.z = v_z1;
  elseif z_axis == 2
    [x_line1 y_line1 corners]  = get_corner(im, "left", false);
    [x_line2 y_line2 corners]  = get_corner(im, "right", false);
    [x_line3 y_line3 corners]  = get_corner(im, "left-1", false);
    [x_line4 y_line4 corners]  = get_corner(im, "right-1", false);
    z_l1 = line_homogeneus(x_line1, y_line1);
    z_l2 = line_homogeneus(x_line2, y_line2);
    z_l3 = line_homogeneus(x_line3, y_line3);
    z_l4 = line_homogeneus(x_line4, y_line4);
    v_z1 = cross(z_l1, z_l2);
    v_z2 = cross(z_l3, z_l4);
    v_z1 = v_z1/v_z1(3);
    v_z2 = v_z2/v_z2(3);
    if debug
      plot(x_line1, y_line1, 'LineWidth',1,'Color','g');
      plot(x_line2, y_line2, 'LineWidth',1,'Color','g');
      plot(x_line3, y_line3, 'LineWidth',1,'Color','g');
      plot(x_line4, y_line4, 'LineWidth',1,'Color','g');
      plot(v_z1(1), v_z1(2),'go');
      plot(v_z2(1), v_z2(2),'go');
    end
    vanishing_points.z = [v_z1 v_z2];

  else
    ME = MException("Vanishing point of z-axis: supported args are 1 or 2");
    throw(ME);
  end

  if debug

    % lets try to plot x, y, z
    [O X Y Z] = get_axis3d(im, 30, 500);
    plot3(O(1), O(2), O(3), 'm');
    plot3([O(1) X(1)], [O(2) X(2)], [O(3) X(3)], 'LineWidth', 1, 'Color','r');
    plot3([O(1) Y(1)], [O(2) Y(2)], [O(3) Y(3)], 'LineWidth', 1, 'Color','b');
    plot3([O(1) Z(1)], [O(2) Z(2)], [O(3) Z(3)], 'LineWidth', 1, 'Color','g');
    text(double(X(1)), double(X(2)), double(X(3)), "x", 'Color', 'r', 'FontSize', 9);
    text(double(Y(1)), double(Y(2)), double(Y(3)), "y", 'Color', 'b', 'FontSize', 9);
    text(double(Z(1)), double(Z(2)), double(Z(3)), "z", 'Color', 'g', 'FontSize', 9);

    s = "out/vanishing_points_x";
    filename = strcat(s, num2str(x_axis), "_y", num2str(y_axis), "_z", num2str(z_axis));
    title(['Vanishing Points: \fontsize{9}'...
      'x-axis: ' num2str(x_axis), ...
      ' y-axis: ', num2str(y_axis), ' z-axis: ', num2str(z_axis)]);
    print(fig, filename, '-dpng');

  end

end
