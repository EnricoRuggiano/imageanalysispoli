function ratio = ...
  cross_ratio(im, debug)

% get my lines
[xVertical1 yVertical1 ~] = get_corner(im, "vertical-1", false);
[xVertical2 yVertical2 ~] = get_corner(im, "vertical-2", false);
[xVertical3 yVertical3 ~] = get_corner(im, "vertical-3", false);
[xVertical4 yVertical4 ~] = get_corner(im, "vertical-4", false);


[xDown1 yDown1 ~] = get_corner(im, "down", false);
[xDown2 yDown2 ~] = get_corner(im, "up-first", false);
[xDown3 yDown3 ~] = get_corner(im, "up-second", false);

vertical1 = [xVertical1' yVertical1'];
vertical2 = [xVertical2' yVertical2'];
vertical3 = [xVertical3' yVertical3'];
vertical4 = [xVertical4' yVertical4'];
down1 = [xDown1' yDown1'];
down2 = [xDown2' yDown2'];
down3 = [xDown3' yDown3'];

l_vertical1 = line_homogeneus(xVertical1, yVertical1);
l_vertical2 = line_homogeneus(xVertical2, yVertical2);
l_vertical3 = line_homogeneus(xVertical3, yVertical3);
l_vertical4 = line_homogeneus(xVertical4, yVertical4);
l_down1 = line_homogeneus(xDown1, yDown1);
l_down2 = line_homogeneus(xDown2, yDown2);
l_down3 = line_homogeneus(xDown3, yDown3);

a = point_homogeneus(l_vertical1, l_down1);
b = point_homogeneus(l_vertical2, l_down1);
c = point_homogeneus(l_vertical3, l_down1);
d = point_homogeneus(l_vertical4, l_down1);
e = point_homogeneus(l_vertical1, l_down2);
f = point_homogeneus(l_vertical2, l_down2);
g = point_homogeneus(l_vertical3, l_down2);
h = point_homogeneus(l_vertical4, l_down2);
i = point_homogeneus(l_vertical1, l_down3);
l = point_homogeneus(l_vertical2, l_down3);
m = point_homogeneus(l_vertical3, l_down3);
n = point_homogeneus(l_vertical4, l_down3);

if debug
 figure();
 imshow(im);
 hold on

 plot(vertical1(:,1), vertical1(:,2), 'LineWidth',1,'Color','y');
 plot(vertical2(:,1), vertical2(:,2), 'LineWidth',1,'Color','y');
 plot(vertical3(:,1), vertical3(:,2), 'LineWidth',1,'Color','y');
 plot(vertical4(:,1), vertical4(:,2), 'LineWidth',1,'Color','y');
 plot(down1(:,1), down1(:,2), 'LineWidth',1,'Color','y');
 plot(down2(:,1), down2(:,2), 'LineWidth',1,'Color','y');
 plot(down3(:,1), down3(:,2), 'LineWidth',1,'Color','y');

 points_x = [a(1) b(1) c(1) d(1) e(1) f(1) g(1) h(1) i(1) l(1) m(1) n(1)];
 points_y = [a(2) b(2) c(2) d(2) e(2) f(2) g(2) h(2) i(2) l(2) m(2) n(2)];
 labels = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "L" "M" "N"};

 plot(points_x, points_y, 'bo');
 text(double(points_x), double(points_y), labels, 'Color', 'b', 'FontSize', 14);
end

% metric
syms z k;
ac = z + k;
bd = z + k;
bc = k;
ad = 2*z + k;

cratio = (ac * bd)/(bc * ad);

% pixels distances
lac = distance(a, c);
lbd = distance(b, d);
lbc = distance(b, c);
lad = distance(a, d);

leg = distance(e, g);
lfh = distance(f, h);
lfg = distance(f, g);
leh = distance(e, h);

cr1 = (lac*lbd)/(lbc * lad);
cr2 = (leg*lfh)/(lfg * leh);

% equation
eq1 = cratio == cr1;
s = solve(eq1);
result = subs(s, k, 1);
r = double(result);

% front facade respect to behind facade
facade = r(r>0);

% ###############################################
% second cross ratio on front facade
[xWindow1 yWindow1 ~] = get_corner(im, "window-vertical-1", false);
[xWindow2 yWindow2 ~] = get_corner(im, "window-vertical-2", false);
window1 = [xWindow1' yWindow1'];
window2 = [xWindow2' yWindow2'];
l_window1 = line_homogeneus(xWindow1, yWindow1);
l_window2 = line_homogeneus(xWindow2, yWindow2);
w1 = point_homogeneus(l_window1, l_down1);
w2 = point_homogeneus(l_window2, l_down1);

a2 = a;
b2 = w1;
c2 = w2;
d2 = b;

if debug
 figure();
 imshow(im);
 hold on

 plot(vertical1(:,1), vertical1(:,2), 'LineWidth',1,'Color','y');
 plot(vertical2(:,1), vertical2(:,2), 'LineWidth',1,'Color','y');
 plot(window1(:,1), window1(:,2), 'LineWidth',1,'Color','y');
 plot(window2(:,1), window2(:,2), 'LineWidth',1,'Color','y');
 plot(down1(:,1), down1(:,2), 'LineWidth',1,'Color','y');

 points_x = [a2(1) b2(1) c2(1) d2(1)];
 points_y = [a2(2) b2(2) c2(2) d2(2)];
 labels = {"A", "B", "C", "D"};

 plot(points_x, points_y, 'bo');
 text(double(points_x), double(points_y), labels, 'Color', 'b', 'FontSize', 14);
end

% metric
syms x z w;
ac2 = x + 1;
bd2 = z + 1;
bc2 = 1;
ad2 = x+z+1;

cratio2 = (ac2 * bd2)/(bc2 * ad2);

% pixels distances
lac2 = distance(a2, c2);
lbd2 = distance(b2, d2);
lbc2 = distance(b2, c2);
lad2 = distance(a2, d2);

cr2 = (lac2 * lbd2)/(lbc2 * lad2);

% equation
eq2 = cratio2 == cr2;
s = solve(eq2);
result = double(subs(s, z, 1));

% a-b respect to c-d
space = result;


if debug
 figure();
 imshow(im);
 hold on

 plot(window1(:,1), window1(:,2), 'LineWidth',1,'Color','y');
 plot(window2(:,1), window2(:,2), 'LineWidth',1,'Color','y');
 plot(vertical2(:,1), vertical2(:,2), 'LineWidth',1,'Color','y');
 plot(vertical3(:,1), vertical3(:,2), 'LineWidth',1,'Color','y');
 plot(down1(:,1), down1(:,2), 'LineWidth',1,'Color','y');


end
