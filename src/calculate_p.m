%
% - case plane xz
%
%      [ x ]        [ iπ jπ kπ oπ ]
% xπ = [ 0 ]  xw =  [  0  0  0  1 ] * xπ
%      [ z ]
%      [ w ]
%
%  so:
%     xw =  [ iπ  kπ oπ ]
%           [  0   0  1 ] * [ x  z  w] ^ T
%
%   u = P * xw = [ K | 0 ] *  [ iπ  kπ oπ ] *  [ x  z  w] ^ T
%                             [  0   0  1 ]
%
%   with:
%      H =  K * [ iπ  kπ oπ ]
%   we have
%      u = H * xπ
%
%   where u is image reference and xw is world reference;

function [P R t M p4] = ...
  calculate_p(K, Hplanar, plane)

if strcmp(plane, 'xz')

  result = inv(K) * Hplanar;

  r1 = result(:, 1);
  r3 = result(:, 2);
  t = result(:, 3);

  r2 = cross(r1, r3);
  r2 = r2/r2(3);

  R = [r1 r2 r3];
  P = K * [R t];
  M = K * R;
  p4 = inv(M) * P(:, 4);

% here put other planes - we do not implement this
else
  ME = MException("Calculate P: plane not implemented (use only xz)");
  throw(ME);
end

end
