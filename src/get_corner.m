function [xline yline corners] = get_corner(im, position, debug)


if strcmp(position, "up-first")
  % get line from these points
  rotate = false;

  % first floor of window
  x1 = 1181;
  y1 = 1755;
  x2 = 1441;
  y2 = 1565;

  ROI1 = [x1 y2 x2-x1 y1-y2];

  % first floor of window
  x3 = 2783;
  y3 = 746;
  x4 = 3130;
  y4 = 480;

  ROI2 = [x3 y4 x4-x3 y3-y4];

elseif strcmp(position, "up-second")

  rotate = false;

  % second floor of window
  x1 = 1214;
  y1 = 1287;
  x2 = 1457;
  y2 = 1106;

  ROI1 = [x1 y2 x2-x1 y1-y2];

  % second floor of window
  x3 = 2561;
  y3 = 424;
  x4 = 2828;
  y4 = 218;

  ROI2 = [x3 y4 x4-x3 y3-y4];

elseif strcmp(position, "up-below")

  % get line from these points
  rotate = false;

  % windows of the below floor
  x1 = 2219;
  y1 = 2258;
  x2 = 2456;
  y2 = 2213;

  ROI1 = [x1 y2 x2-x1 y1-y2];

  % windows of the below floor
  x3 = 2821;
  y3 = 2043;
  x4 = 2992;
  y4 = 1872;

  ROI2 = [x3 y4 x4-x3 y3-y4];

elseif strcmp(position, "up-last")

  % get line from these points
  rotate = true;
  p1 = [1218; 1052];
  p2 = [2652; 1];
  %rotate = false;

  % get region from these rotated points
  x1 = 1513;
  y1 = 1800;
  x2 = 1900;
  y2 = 1868;

  ROI1 = [x1 y2 x2-x1 abs(y1-y2)];

  %second region of the rotated image
  x3 = 165;
  y3 = 1593;
  x4 = 465;
  y4 = 1611;

  ROI2 = [x3 y4 x4-x3 abs(y3-y4)];

  elseif strcmp(position, "vertical-1")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 1003;
    y1 = 2685;
    x2 = 1033;
    y2 = 2783;

    ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 1188;
    y3 = 1010;
    x4 = 1211;
    y4 = 1088;

    ROI2 = [x3 y4 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "vertical-2")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 1715;
    y1 = 2356;
    x2 = 1727;
    y2 = 2073;

    ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 1562;
    y3 = 834;
    x4 = 1597;
    y4 = 798;

    ROI2 = [x3 y4 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "vertical-4")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 3929;
    y1 = 1177;
    x2 = 3927;
    y2 = 1056;

    ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 2691;
    y3 = 11;
    x4 = 2712;
    y4 = 70;

    ROI2 = [x4 y3 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "vertical-3")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 2966;
    y1 = 1768;
    x2 = 2981;
    y2 = 1614;

    ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 2192;
    y3 = 338;
    x4 = 2335;
    y4 = 468;

    ROI2 = [x3 y3 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "window-vertical-1")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 1143;
    y1 = 2375;
    x2 = 1166;
    y2 = 2331;

    ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 1193;
    y3 = 1791;
    x4 = 1214;
    y4 = 1751;

    ROI2 = [x3 y4 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "window-vertical-2")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 1378;
    y1 = 2574;
    x2 = 1410;
    y2 = 2200;

    ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 1397;
    y3 = 986;
    x4 = 1409;
    y4 = 925;

    ROI2 = [x3 y4 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "left-vertical-1")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 52;
    y1 = 1998;
    x2 = 43;
    y2 = 1756;

    ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 633;
    y3 = 638;
    x4 = 617;
    y4 = 550;

    ROI2 = [x3 y4 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "left-vertical-2")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 403;
    y1 = 2143;
    x2 = 367;
    y2 = 2061;

    ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 732;
    y3 = 957;
    x4 = 773;
    y4 = 1029;

    ROI2 = [x3 y3 abs(x4-x3) abs(y3-y4)];

    elseif strcmp(position, "left-vertical-3")

    % get line from these points
    rotate = false;

    % get region from these rotated points
    x1 = 643;
    y1 = 2186;
    x2 = 659;
    y2 = 2333;

    ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

    %second region of the rotated image
    x3 = 942;
    y3 = 839;
    x4 = 968;
    y4 = 921;

    ROI2 = [x3 y3 abs(x4-x3) abs(y3-y4)];


elseif strcmp(position, "down")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 1161;
  y1 = 2235;
  x2 = 1385;
  y2 = 2132;
  ROI1 = [x1 y1 x2-x1 y1-y2];

  %second region of the rotated image
  x3 = 3183;
  y3 = 1166;
  x4 = 3462;
  y4 = 935;
  ROI2 = [x3 y4 x4-x3 y3-y4];

  elseif strcmp(position, "right-1")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 3000;
  y1 = 10;
  x2 = 3090;
  y2 = 50;

  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 3132;
  y3 = 376;
  x4 = 3153;
  y4 = 326;

  ROI2 = [x4 y4 abs(x4-x3) abs(y3-y4)];

elseif strcmp(position, "left")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 1043;
  y1 = 2290;
  x2 = 1090;
  y2 = 2310;

  ROI1 = [x1 y1 x2-x1 y2-y1];

  %second region of the rotated image
  x3 = 419;
  y3 = 1972;
  x4 = 600;
  y4 = 2000;

  ROI2 = [x3 y3 x4-x3 y4-y3];

elseif strcmp(position, "right")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 3552;
  y1 = 828;
  x2 = 3588;
  y2 = 801;
  ROI1 = [x1 y1 x2-x1 y1-y2];

  %second region of the rotated image

  x3 = 3273;
  y3 = 46;
  x4 = 3369;
  y4 = 119;
  ROI2 = [x3 y3 x4-x3 y4-y3];

  elseif strcmp(position, "left-1")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 837;
  y1 = 703;
  x2 = 921;
  y2 = 828;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 1124;
  y3 = 982;
  x4 = 1165;
  y4 = 1025;

  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "window-up")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 422;
  y1 = 2012;
  x2 = 492;
  y2 = 1962;
  ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 3372;
  y3 = 145;
  x4 = 3396;
  y4 = 55;

  ROI2 = [x3 y4 abs(x4-x3) abs(y4-y3)];


% affine image
elseif strcmp(position, "affine-down")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 1920;
  y1 = 4000;
  x2 = 2508;
  y2 = 3804;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 2208;
  y3 = 845;
  x4 = 2377;
  y4 = 616;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)] *2.2;

  elseif strcmp(position, "affine-right")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 5750;
  y1 = 1501;
  x2 = 5889;
  y2 = 1306;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 5164;
  y3 = 692;
  x4 = 4547;
  y4 = 115;
  ROI2 = [x4 y4 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "affine-y1")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 1837;
  y1 = 4051;
  x2 = 1957;
  y2 = 4190;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 1604;
  y3 = 1385;
  x4 = 1652;
  y4 = 1466;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "affine-y2")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 5745;
  y1 = 1246;
  x2 = 5846;
  y2 = 1410;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 3333;
  y3 = 44;
  x4 = 3422;
  y4 = 72;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "affine-up")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 1861;
  y1 = 2807;
  x2 = 2235;
  y2 = 2479;
  ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 4176;
  y3 = 1122;
  x4 = 4525;
  y4 = 782;
  ROI2 = [x3 y4 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "affine-left")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 662;
  y1 = 2990;
  x2 = 1103;
  y2 = 3490;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 1912;
  y3 = 4266;
  x4 = 2000;
  y4 = 4072;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "metric-right")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 5853;
  y1 = 1988;
  x2 = 5837;
  y2 = 1815;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 5250;
  y3 = 1114;
  x4 = 4950;
  y4 = 570;
  ROI2 = [x4 y4 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "metric-left")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 950;
  y1 = 3300;
  x2 = 1100;
  y2 = 3850;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 1911;
  y3 = 4800;
  x4 = 2000;
  y4 = 4950;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "metric-down")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 2053;
  y1 = 4828;
  x2 = 2485;
  y2 = 4422;
  ROI1 = [x1 y2 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 5179;
  y3 = 2630;
  x4 = 5738;
  y4 = 2103;
  ROI2 = [x3 y4 abs(x4-x3) abs(y4-y3)];

  elseif strcmp(position, "metric-up")

  % get line from these points
  rotate = false;

  % get region from these rotated points
  x1 = 2047;
  y1 = 3226;
  x2 = 2450;
  y2 = 2910;
  ROI1 = [x1 y1 abs(x2-x1) abs(y1-y2)];

  %second region of the rotated image
  x3 = 4347;
  y3 = 1545;
  x4 = 4843;
  y4 = 1175;
  ROI2 = [x3 y3 abs(x4-x3) abs(y4-y3)];

else
  % get line from these points
  p1 = [2213; 317.7];
  p2 = [2652; 1];

  % get region from these rotated points
  x1 = 1615;
  y1 = 1792;
  x2 = 1850;
  y2 = 1815;
  ROI = [x1 y1 x2-x1 y2-y1];
end

% get image rotated
if rotate
  points = [p1 p2];
  [rotatedImage R] = rotate_image(im, points, false);

  % get corners
  rotated_corners1 = detect_corners(rotatedImage, 5, 1, true, ROI1, false);
  rotated_corners2 = detect_corners(rotatedImage, 5, 1, true, ROI2, false);

  % union of the corners
  coords1 = rotated_corners1.Location;
  coords2 = rotated_corners2.Location;
  row_rotated_corners = [coords1', coords2'];
  rotated_corners = row_rotated_corners';

  % add dimension
  thirdColumn = ones(size(rotated_corners, 1), 1);
  rotated_corners = [rotated_corners, thirdColumn];

  % get original corners
  tcorners = R' * rotated_corners';
  corners = tcorners';
else
  rotated_corners1 = detect_corners(im, 5, 1, true, ROI1, false);
  rotated_corners2 = detect_corners(im, 5, 1, true, ROI2, false);
  coords1 = rotated_corners1.Location;
  coords2 = rotated_corners2.Location;
  row_rotated_corners = [coords1', coords2'];
  rotated_corners = row_rotated_corners';

  % add dimension
  thirdColumn = ones(size(rotated_corners, 1), 1);
  rotated_corners = [rotated_corners, thirdColumn];

  % get original corners
  corners = rotated_corners;

end
% perform linear regression on corners
x = corners(:, 1);
y = corners(:, 2);
coeff = polyfit(x, y, 1);
%xline = linspace(min(x), max(x), 200);
xline = linspace(0, size(im, 2), 200);
yline = coeff(1) * xline + coeff(2);

%plot
if debug
  figure();
  imshow(im);
  hold on;
  plot(x, y, 'ro');
  plot(xline, yline, 'y');
end
end
