% - Perform edge detection.
% Edge detection is performed after a gamma correction and an histeq on the
% input image

function [im_edge, c_im_edge] = ...
detect_edge(img_in, algo, gamma, eq, debug)

% default parameters
if nargin < 4
  gamma = 1;
  algo = 'canny';
  eq = false;
end

% to grayscale
gray = rgb2gray(img_in);

% do some filtering
im = imadjust(gray, [], [], gamma);
if eq
  im = histeq(im);
  str_eq = 'true';
else
  str_eq = 'false';
end

% edge algorithm
im_edge = edge(im, algo);
c_im_edge = imcomplement(im2bw(im_edge, graythresh(gray)));

img_out = [im_edge, c_im_edge];

% display
if debug
  fig = figure();
  imshow(im_edge);

%convert bool to str
  title(['Detect Edges:\fontsize{9} Algorithm: ', algo, ...
   ' Gamma: ', num2str(gamma), ' Histeq: ', str_eq]);
  print(fig, "out/edges", '-dpng');

end
end
