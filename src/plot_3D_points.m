function plot_3D_points(xz_points, debug)

fig = figure();
hold on;

% xz plane
A = xz_points(:, 1);
B = xz_points(:, 2);
C = xz_points(:, 3);
D = xz_points(:, 4);
E = xz_points(:, 5);
F = xz_points(:, 6);
G = xz_points(:, 7);

% plot all points
X = xz_points(1, :);
Y = xz_points(2, :);
Z = xz_points(3, :);
plot3(X, Y, Z, 'or');

% plot lines
plot3([A(1), B(1)], [A(2), B(2)], [A(3), B(3)], 'LineWidth',1,'Color','r');
plot3([B(1), C(1)], [B(2), C(2)], [B(3), C(3)], 'LineWidth',1,'Color','r');
plot3([C(1), D(1)], [C(2), D(2)], [C(3), D(3)], 'LineWidth',1,'Color','r');
plot3([D(1), E(1)], [D(2), E(2)], [D(3), E(3)], 'LineWidth',1,'Color','r');
plot3([E(1), F(1)], [E(2), F(2)], [E(3), F(3)], 'LineWidth',1,'Color','r');
plot3([F(1), G(1)], [F(2), G(2)], [F(3), G(3)], 'LineWidth',1,'Color','r');
labels = {"A", "B", "C", "D", "E", "F", "G"};
text(double(X), double(Y), double(Z), labels, 'Color', 'r', 'FontSize', 14);


if debug
title(['3D reconstruction']);
print(fig, "out/3d_reconstruction", '-dpng');
end


end
