% clean environment
close all;
clear;
clc;

% load functions on src subdirectories
addpath("src");

% load the input image and its black and gray version
im = im2double(imread('homework.jpg'));
gray = rgb2gray(im);

% ###############################################
% 1 - find edges and lines

[im_edges, c_im_edge] = detect_edge(im, 'canny', 0.7, true, true);
detect_lines(im_edges, 8, 100, true);
detect_corners(im, 5, 1, true, [1 1 size(im, 2) size(im, 1)], true);

% ###############################################
%  2.1 - Find two horizontal vanishing points and one vertical vanishing point

vanishing_points = get_vanishing(im, 2, 1, 0, true);

% ###############################################
% 2.2 - rectify section

[im_xz, H_xz, ~] = rectify6(im, 100, true);
H = H_xz;

% ###############################################
% 2.3 - calibrate camera

K = estimate_K4(H, im, true);

% ###############################################
% 2.4 - rectification from K

vanishing_points = get_vanishing(im, 1, 1, 1, false);
[im_out H_facade ] = rectify_from_K2(im, K, vanishing_points, "z", "y", true, 10, true);

% ###############################################
% 2.5 - localization

% plane xz
% we use the previous rectification which is connected to plane xz
[~, K_3const]  = estimate_K(im, false);
[P R t M p4] = calculate_p(K_3const, H_xz,'xz');
xz_points = locate_points(im_xz, P, M, R, t, 'xz', true);

% plot
plot_3D_points(xz_points, true);
