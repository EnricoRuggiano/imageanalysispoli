# IAC Homework

* Take a look to ```HOMEWORK ASSIGNMENT.pdf``` for the goals of this homework.
* Take a look to ```report.pdf``` for the procedures to solve the homework.
* On MATLAB run the  ```homework.m``` script.
* Source code used is on ```src/``` folder.
* Output pictures and results are saved on ```out/``` folder.
* To build the report ```./build_report.sh```.
* To build the archive ```./build_archive.sh``` (ZIP).
