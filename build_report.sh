pandoc -s -V geometry:margin=1.5in -V geometry:a4paper -o temp.pdf  -f markdown-latex_macros report_utils/report.md && \
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=report.pdf \
report_utils/IAC_cover.pdf temp.pdf && \
rm temp.pdf
