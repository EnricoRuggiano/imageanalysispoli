
# Image Features extraction and selection

The given image presents two visible regions with different shades, one darker on the right and one lighter on the left. To outperform a good extraction solution we added two preprocessing operation on the image: Gamma adjust, depending on one parameter $\gamma$, and Histogram equalization.

Then an algorithm was used for the specific feature extraction.  

### Edge Detection

For edge detection we tried three different algorithms: *Canny, Prewitt, Sobel*.
*Canny* was the best one and after some parameter tuning on the preprocessing operations we found that the best result was obtained using Gamma adjust with $\gamma = 0.7$ and with histogram equalization.

![Edge detection result.](out/edges.png)

\newpage

### Corner Detection

For corner detection we used the  *Harris Feature* algorithm. This algorithm depends on a parameter $\psi$ which is the filter width. In this case the filter width used was the default one $\psi=5$.

A good observation was that with matlab is possible to detect the *Harris Feature* also on a small region of the image (Region of Interest) and this let us possible to detect all the lines useful for the Geometry analysis.

Our main idea is to perform Corner Detection on two different regions, preprocessed with a gamma adjust and an histogram equalization, detect the corners and then perform Linear regression. All the lines used on the Geometry analysis were extracted using this technique.

The best result on the whole image was obtained with $\psi=5$, $\gamma = 1$ and with Histogram equalization.

![Corner detection result.](out/corners.png)

![Example of line extraction using corner detection on two regions and then performing linear regression on the corners.](out/example_corner_detection.jpg)

\newpage

### Line Detection

For Line Detection we used the *Hough lines* algorithm. This algorithm uses the edge calculated before to perform line detection.
It depends on two parameter FillGap and MinLength. The result was pretty poor and the lines detected in this way were not so interesting for the Geometry analysis. So we did not take too much effort on tuning those parameters because we preferred to use another technique to extract lines (see Corner Detection).

Anyway we observed that a good result is achieved using *FillGap = 8* and *MinLength = 100* where almost *65* lines were extracted.

![Hough Line detection result.](out/lines.png)


\newpage

# Geometry

For all the Geometry analysis we assumed that in the 3D world z axis is parallel to the lateral facades planes of the building, x axis is parallel to the front facade plane and that y is the vertical axis.
This is the same convention used in computer graphics applications.     

## 2.1 Find two horizontal vanishing points and one vertical vanishing point

We just intersect two pair of horizontal lines which are parallel in the metric world and one pair of vertical lines.

In figure 5 the solutions are plotted and it is also visible the convention used to identify the axis directions.

The resulted vanishing points are:

$$v_{x1} = \begin{bmatrix}
    -1.5919 e+04  \\
    1.2317 e+04   \\
    1    
\end{bmatrix}$$

$$v_{x2} = \begin{bmatrix}
    -0.8313 e+04  \\
    0.7506 e+04   \\
    1    
\end{bmatrix}$$

$$v_{y} = \begin{bmatrix}
    1.447 e+03  \\
    -0.995 e+03  \\
    1    
\end{bmatrix}$$

where $v_{x1}, v_{x2}$ are the two horizontal vanishing point and $v_{y}$ is the vertical one.

![Vanishing points found.](out/vanishing_points_x2_y1_z0.png)

## 2.2 Horizontal rectification

Here we tried some approaches such the stratified one, but the best one found was calculating H from 4 points.
One probably explanation for this may be that the line of the infinite passing from the vanishing point of direction x is pretty noisy.

So the idea was to map 4 points A B C D of a quadrilater to a rectangle.   

![Region to rectify.](out/rectify_region.png)


To know the metric coordinates of the rectangle we needed to know the metric distance between the two lateral facade and the one from frontal window to the first lateral window.
To estimate those we used the cross ratio two times. One on the frontal facade and another one on the lateral facade.

From the assumption that the window width is 1 meter, we found that the distance between the two lateral facade is 9.3 meter and the distance between the two consecutive window is 2.7 meter.


### 1 - Cross Ratio on the Frontal Facade

Referring to Figure 7 we applied the following relations to estimate the distance between the two lateral facades.

Using the cross ratio definition we have:

$$\begin{array} {rcl}
  crr_{1}(A, D, E, F) & = &\frac{\overline{AE} \cdot \overline{DF}}{\overline{DE} \cdot \overline{AF}}  \\\\
  crr_{2}(B, C, D, E) & = & \frac{\overline{BD} \cdot \overline{CE}}{\overline{CD} \cdot \overline{BE}}  \\\\
  crr_{3}(A, B, C, D) & = & \frac{\overline{AC} \cdot \overline{BD}}{\overline{BC} \cdot \overline{AD}}  
\end{array}$$

where:
$$\begin{array}{rcl}
  \overline{AB} & = & x \\
  \overline{BC} & = & w \\
  \overline{CD} & = & z \\
  \overline{DE} & = & k
\end{array}$$

where *w* is the window metric width that for assumption is *1 meter*.
Knowing also that $\overline{AD} = \overline{EF}$ we can solve the system and calculate $\overline{AF}$ in function of *w*;  

![Cross ratio applied on the frontal facade.](out/cross_ratio1.png)

\newpage

### 2 - Cross Ratio on the Lateral Facade

Referring to Figure 8 we applied the following relation to estimate the distance between the two consecutives window.

Using the cross ratio definition we have:

$$\begin{array} {rcl}
  crr_{1}(A, B, C, D) & = &\frac{\overline{AC} \cdot \overline{BD}}{\overline{AD} \cdot \overline{BC}}  \\
\end{array}$$

where:
$$\begin{array}{rcl}
  \overline{AB} & = & f \\
  \overline{BC} & = & w \\
  \overline{CD} & = & f
\end{array}$$

we can solve the system and calculate $\overline{BD}$ in function of *w*. Here we also assumed that $\overline{CD} = \overline{AB}$.

![Cross ratio applied on the lateral facade.](out/cross_ratio2.png)

\newpage

### 3 - Rectification

From the metric values calculated before with cross ratio we can calculate H as to map the points *A, B, C, D* to their respective metric *A', B', C', D'* as that the quadrilater *A'B'C'D'* is rectangle, such that:

$$\begin{array} {rcl}
  \overline{A^{'}B^{'}} & = & d_{1} \\
  \overline{A^{'}D^{'}} & = & d_{2}
\end{array}$$

where $d_{1}$ is the distance calculated in the step 1 and $d_{2}$ is the distance calculated in the step 2.

The result is shown in the figure 9.   

![Horizontal rectification.](out/rectify_xz.png)

\newpage


## 2.3 Estimate Calibration Matrix

To estimate the Calibration Matrix we first have to calculate the Image of the Absolute Conic $\omega$ and then apply the Cholenski decomposition.

Using the assumption of the homework (zero skew, not natural)

$$\omega = \begin{bmatrix}
    w_{1}  &  0   & w_{4}   \\
    0   &  w_{2}  & w_{5}   \\
    w_{4}   &  w_{5}  & w_{3}   
\end{bmatrix}$$

so we needed 4 constraint to calculate $\omega$.

In our specific case a $\omega$ positive definite was not found for all the possible combination of the five constraint calculated.

So we used a technique to approximate a matrix to the nearest positive definite matrix and then using Cholenski decomposition we calculated the K approximated.

In the end we compared all the K found with NPD algorithm with the K' found with natural camera assumption ($\omega$ in that case was positive definite). The best K was the one which was the nearest to K'.   


\newpage

### Calculating $\omega$ with not Natural Camera assumption

The constraints used to find $\omega$ with not natural camera assumption are the following:

$$\begin{array}{lcl}
  v_{x}^{T} \cdot \omega \cdot v_{y} & = & 0 \\
  v_{x}^{T} \cdot \omega \cdot v_{z} & = & 0 \\
  v_{y}^{T} \cdot \omega \cdot v_{z} & = & 0 \\
  h_{1}^{T} \cdot \omega \cdot h_{2} & = & 0 \\
  h_{1}^{T} \cdot \omega \cdot h_{1} - h_{2}^{T} \cdot \omega \cdot h_{2} & = & 0
\end{array}$$

where:

$v_{x}$ is the vanishing point along the direction x.
$v_{y}$ is the vanishing point along the direction y.
$v_{z}$ is the vanishing point along the direction z.
$h_{1}$ and $h_{2}$ are the first and the second column of the $H$ calculated on the rectification step.

To find the $\omega$ we just need four constraint from the above five constraints. However all of them were used.

In our homework, all the $\omega$, solution of all the possible system formed by the combination of those five equations,
were always not positive definite, so the Cholenski Decomposition was not possible.

To make it possible to calculate $K$, we approximated the $\omega$ to the nearest positive definite matrix (https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd).


### 2 - Calculating $\omega$ with Natural Camera assumption

With natural camera assumption $\omega$ has the following form:

$$\omega = \begin{bmatrix}
    w_{1}  &  0   & w_{2}   \\
    0   &  w_{1}  & w_{3}   \\
    w_{2}   &  w_{3}  & w_{4}   
\end{bmatrix}$$

The constraints used to find $\omega$ with natural camera assumption are the following:

$$\begin{array}{lcl}
  v_{x}^{T} \cdot \omega \cdot v_{y} & = & 0 \\
  v_{x}^{T} \cdot \omega \cdot v_{z} & = & 0 \\
  v_{y}^{T} \cdot \omega \cdot v_{z} & = & 0 \\
\end{array}$$

So only the information of the vanishing points are used.

The $\omega$ calculated with this constraints was positive definite and so a Cholenski Decomposition was possible.
The result $K^{'}$ was helpful to estimate the best $K$ with not natural camera assumption.

### 3 - Estimation of the best $\omega$ for the $K$

For each $\omega$ calculated on the step 1 we calculated the nearest positive definite $\omega_{approx}$ and then we applied Cholenski decomposition to get the $K_{approx}$.

Then we compared the $K_{approx}$ with the $K^{'}$ calculated on the step 2 using this expression

$$\begin{array}{lcl}
  score = \frac{2 \cdot \Delta\alpha_{x} + 2 \cdot \Delta\alpha_{y} + \Delta x_{0} + \Delta y_{0}}{4}  
\end{array}$$

where:

$$\begin{array}{lcl}
  \Delta\alpha_{x} = ||\alpha^{'}| - |\alpha_{x, approx}|| \\
  \Delta\alpha_{y} = ||\alpha^{'}| - |\alpha_{y, approx}|| \\
  \Delta x_{0} = ||x_{0}^{'}| - |x_{0, approx}|| \\
  \Delta y_{0} = ||y_{0}^{'}| - |y_{0, approx}||
\end{array}$$

and that:

$$ K^{'} =
\begin{bmatrix}
    \alpha^{'}  &  0   & x_{0}^{'}   \\
    0   &  \alpha^{'}  & y_{0}^{'}   \\
    0   &  0  & 1   
\end{bmatrix}$$

$$ K_{approx} =
\begin{bmatrix}
    \alpha_{x, approx}  &  0   & x_{0, approx}   \\
    0   &  \alpha_{y, approx}  & y_{0, approx}   \\
    0   &  0  & 1  
\end{bmatrix}$$

### 4 - Experimental Result of the Estimation of the best $\omega$ for the $K$

Here we report our experimental results obtained in our approach.

\begin{table}[h!]
\begin{center}
\begin{tabular}{l|rrrr}
      \hline
      Equations & $\alpha_{x, approx}$ & $\alpha_{y, approx}$ & $x_{0, approx}$ & $y_{0, approx}$ \\
      \hline
      1, 2, 3, 4 & 3312.7 & 199.4 & -4436.8 & 142.2 \\
      1, 2, 3, 5 & 1661.9 & 106.4 & -2362.9 & 75.7 \\
      1, 2, 4, 5 & 312.3 & 137.6 & 1363.9 & 134.1 \\
      1, 3, 4, 5 & 382.9 & 2.6 & 1689.8 & 2.3 \\
      2, 3, 4, 5 & 558.6 & 1.5 & 2468.6 & 1.3 \\
      \hline
\end{tabular}
\end{center}
\end{table}

In the end the best $K$ found with this approach is:
  $$ \alpha_{x} = 3312.7 \alpha_{y} = 199.4   x_{0} = -4436.8   y_{0} = 142.2 $$

while the $K^{'}$ calculated with natural camera assumption is:
$$ \alpha_{x}^{'} = 2879.3   \alpha_{y}^{'} = 2879.3    x_{0}^{'} = 2276.8   y_{0}^{'} = 1087.8 $$


## 2.4 Rectify with K

We used the previous information of the $\omega$ and a line of the infinite to calculate the image of the circular points. From this we calculated the image of the dual conic of the circular points performing the outer product on them and then we applied SVD to estimate the H.

In this case we chose the line of the infinite passing from the vanishing points along the directions of y anz z to rectify the lateral facade (yz plane).

![Lateral rectification.](out/rectify_yz.png)

### Calculating the H from the image of the circular points

The idea is to solve the following system:

$$\begin{array}{lcl}
I^{T} \cdot \omega \cdot I & = & 0 \\
\ell_{\infty}^{T} \cdot I = & 0
\end{array}$$

The result are two conjugate $I_{1}, I_{2}$ solution of $I$.
Then we calculate the image of the dual conic of the circular points.

$$\begin{array}{lcl}
C_{\infty} = & OuterProduct(I_{1}, I_{2})
\end{array}$$

where $OuterProduct$ is defined here https://en.wikipedia.org/wiki/Outer_product.

Then we apply SVD on the $C_{\infty}$ and we get H

$$\begin{array}{lcl}
SVD(C_{\infty}) = U \cdot C \cdot V^{T} \\
H = U
\end{array}$$

\newpage

## 2.5 Localization

To solve the localization problem we need to calculate $P$.

First step is to use the $H$ which rectifies the plane xz and the K to calculate R and t.
Taking care of our 3d axis convention (y is up, z is the depth and x is the horizontal direction), we will have the following result:

$$\begin{array}{lcl}
\begin{bmatrix} r_{1} & r_{3} & t \end{bmatrix} = K ^{-1} \cdot H \\\\
r_{2} = cross(r_{1}, r_{3}) \\\\
R = \begin{bmatrix} r_{1} & r_{2} & r_{3} \end{bmatrix}
\end{array}$$

Then we can calculate P using the following relation:
$$ P = K \cdot \begin{bmatrix} R & t \end{bmatrix} $$

Therefore with P we can calculate all the elements needed for localization.

Last step is to sample some points in the rectified image and map them in the 3d space using this expression:

$$ x_{3D world} = PseudoInverse(P) \cdot x_{2D image} $$

where:
  $$ PseudoInverse(P) = \begin{bmatrix} P \\ 0 & 0 & 0 & 1\end{bmatrix}^{-1} $$

### 3D points mapping  

Here we sampled some significant points of the facade and we mapped them in the 3D space according the steps specified.

On Figure 11 are visible the sampled points and on Figure 12 the result in the 3D space.

![Points to localize in 3d space.](out/3d_reconstruction_points.png)

![Position of the points in 3d space.](out/3d_reconstruction.png)


\newpage

# References

#. Outer Procuct:  https://en.wikipedia.org/wiki/Outer_product
#. Nearest SPD matrix: https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd
